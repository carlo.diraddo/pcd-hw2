package pcd.ass2.wordcounter.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;
import io.reactivex.rxjava3.schedulers.Schedulers;
import pcd.ass2.wordcounter.producers.PageProducer;
import pcd.ass2.wordcounter.producers.RecursiveDirectoryPDFProducer;
import pcd.ass2.wordcounter.producers.WordCountProducer;
import pcd.ass2.wordcounter.structures.data.Page;

public class ObservableTasksHandler {

	private final List<String> excludedWords;

	public ObservableTasksHandler(List<String> excludedWords) {
		this.excludedWords = excludedWords;
	}

	/**
	 * Recursively reads a folder for readable pdf files
	 * 
	 * @param baseDirectory
	 * @return
	 */
	public Flowable<File> recursiveFolderPDFSearchObservable(File baseDirectory) {
		FlowableOnSubscribe<File> producer = new RecursiveDirectoryPDFProducer(baseDirectory);
		return Flowable.create(producer, BackpressureStrategy.BUFFER).subscribeOn(Schedulers.io());
	}

	/**
	 * Reads a file and produces pages of fixed size
	 * 
	 * @param file
	 * @return
	 */
	public Flowable<Page> pageProducer(File file) {
		FlowableOnSubscribe<Page> producer = new PageProducer(file);
		return Flowable.create(producer, BackpressureStrategy.BUFFER).subscribeOn(Schedulers.computation());
	}

	/**
	 * Reads a file and produces pages of fixed size
	 * 
	 * @param file
	 * @return
	 */
	public Flowable<Map<String, Integer>> wordCountProducer(Page page) {
		FlowableOnSubscribe<Map<String, Integer>> producer = new WordCountProducer(page, excludedWords);
		return Flowable.create(producer, BackpressureStrategy.BUFFER).subscribeOn(Schedulers.computation());
	}

}
