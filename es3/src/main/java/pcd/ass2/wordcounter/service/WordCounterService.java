package pcd.ass2.wordcounter.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import io.reactivex.rxjava3.disposables.Disposable;
import pcd.ass2.gui.WordCounterParameters;
import pcd.ass2.logging.Log;
import pcd.ass2.wordcounter.structures.data.WordCounterEntry;
import pcd.ass2.wordcounter.structures.data.WordCounterMap;

public class WordCounterService {

	private final int wordsNumber;
	private final File baseDirectory;
	private final File exclusionFile;

	private final List<String> excludedWords;
	private final WordCounterMap wordCountDictionary;

	private ObservableTasksHandler tasksHandler;

	private Disposable countFlow;

	private final JTextArea outputTextArea;
	private final JTextField wordCountField;
	private final JButton startButton;

	private Instant jobStart;

	public WordCounterService(final WordCounterParameters parameters, JTextArea outputTextArea,
			JTextField wordCountField, JButton startButton) {
		this.wordsNumber = parameters.getMostFrequentWordsNumber();
		this.baseDirectory = parameters.getDirectory();
		this.exclusionFile = parameters.getExclusionFile();
		this.excludedWords = new LinkedList<>();

		this.wordCountDictionary = new WordCounterMap(wordsNumber);
		this.outputTextArea = outputTextArea;
		this.wordCountField = wordCountField;
		this.startButton = startButton;


	}

	public void compute() {
		outputTextArea.setText("Reading files, please wait...");
		countWordOccurencies();
	}

	public void onDone() {
		Instant countFinish = Instant.now();
		Log.info(String.format("Finished Counting Words in %s ms", Duration.between(jobStart, countFinish).toMillis()),
				"MAIN");
		Log.info("Output: counted " + wordCountDictionary.getWordsCounted() + " top words "
				+ wordCountDictionary.getTopWords(), "MAIN");
		outputTextArea.setText("-----Done Counting-----\n" + outputTextArea.getText());
		startButton.setEnabled(true);
	}

	public void stopCount() {
		if (this.countFlow != null && tasksHandler != null) {
			Log.info("Computations stopped", "MAIN");
			this.countFlow.dispose();
			this.countFlow = null;
			outputTextArea.setText("-----Stopped Counting-----\n" + outputTextArea.getText());
			onDone();
		} else {
			Log.info("Cannot stop count, no word count is in progress", "MAIN");
		}
	}

	private void countWordOccurencies() {
		resetCount();
		loadExclusionFile();
		startAsyncFlow();
	}

	private void startAsyncFlow() {
		tasksHandler = new ObservableTasksHandler(excludedWords);

		this.countFlow = tasksHandler.recursiveFolderPDFSearchObservable(baseDirectory)
				.flatMap(tasksHandler::pageProducer)
				.flatMap(tasksHandler::wordCountProducer)
				.subscribe(this::handleJobResult, Throwable::printStackTrace, this::onDone);
	}

	private void resetCount() {
		wordCountDictionary.clear();
		excludedWords.clear();
		jobStart = Instant.now();
		Log.info("Job started", "MAIN");
	}

	private void loadExclusionFile() {
		if (exclusionFile != null) {
			try {
				String content = new String(Files.readAllBytes(Paths.get(exclusionFile.getPath())));
				excludedWords.addAll(Arrays.asList(content.split("\n")));
				List<String> tmp = excludedWords.stream().map(String::trim).map(String::toLowerCase)
						.collect(Collectors.toList());
				excludedWords.clear();
				excludedWords.addAll(tmp);
				Log.info(String.format("Excluded words file found: %s", excludedWords), "MAIN");
			} catch (IOException e) {
				throw new IllegalStateException("IO Exception encountered: " + e);
			}
		} else {
			Log.info("Excluded words file was not provided", "MAIN");
		}
	}

	private Map<String, Integer> handleJobResult(Map<String, Integer> result) {
		wordCountDictionary.registerWordOccurences(result);
		updateGui();
		return result;
	}

	private void updateGui() {
		outputTextArea.setText("");
		for (WordCounterEntry word : wordCountDictionary.getTopWords()) {
			outputTextArea.append(String.format("%s - %s %n", word.getWord(), word.getWordOccurency()));
		}
		wordCountField.setText(String.valueOf("Words Counted: " + wordCountDictionary.getWordsCounted()));

	}

}
