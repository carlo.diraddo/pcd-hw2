package pcd.ass2.wordcounter.structures.data;

import java.util.Map;

public class WordCountResult {

	private final Map<String, Integer> wordCounts;

	public WordCountResult(Map<String, Integer> wordCounts) {
		super();
		this.wordCounts = wordCounts;
	}

	public Map<String, Integer> getWordCounts() {
		return wordCounts;
	}

}
