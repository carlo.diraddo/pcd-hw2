package pcd.ass2.wordcounter.structures.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class WordCounterMap {

	private final Map<String, Integer> internalMap;

	private final int wordsNumber;

	private int wordsCounted;
	private List<WordCounterEntry> topWords;

	public WordCounterMap(int wordsNumber) {
		this.internalMap = new HashMap<>();
		this.wordsNumber = wordsNumber;
		this.topWords = new ArrayList<>();

		this.wordsCounted = 0;
	}

	public void registerWordOccurences(Map<String, Integer> countMap) {

		for (Entry<String, Integer> entry : countMap.entrySet()) {
			String word = entry.getKey();
			Integer newCount = entry.getValue();

			Integer wordOccurencies = internalMap.get(word);
			if (wordOccurencies != null) {
				internalMap.put(word, wordOccurencies + newCount);
			} else {
				internalMap.put(word, newCount);
			}
			wordsCounted += newCount;
		}

		topWords = computeTopWords();
	}

	private List<WordCounterEntry> computeTopWords() {
		List<WordCounterEntry> words = new LinkedList<>();
		List<Entry<String, Integer>> sortedEntries = sortedInternalMapEntries();

		for (int i = 0; i < wordsNumber; i++) {
			Entry<String, Integer> entry = sortedEntries.get(i);
			words.add(new WordCounterEntry(entry.getKey(), entry.getValue()));
		}

		return words;
	}

	private List<Entry<String, Integer>> sortedInternalMapEntries() {
		List<Entry<String, Integer>> list = new LinkedList<>();
		list.addAll(internalMap.entrySet());
		list.sort((a, b) -> b.getValue().compareTo(a.getValue()));
		return list;
	}

	public void clear() {
		internalMap.clear();
	}

	public int getWordsCounted() {
		return wordsCounted;
	}

	public List<WordCounterEntry> getTopWords() {
		return topWords;
	}

}
