package pcd.ass2.wordcounter.producers;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.rxjava3.core.FlowableEmitter;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;
import pcd.ass2.logging.Log;
import pcd.ass2.parser.PdfParser;
import pcd.ass2.parser.PdfParserException;
import pcd.ass2.wordcounter.structures.data.Page;

public class PageProducer implements FlowableOnSubscribe<Page> {

	private static final int PAGE_SIZE = 5000;

	private static final String NAME = "READ_PDF " + Thread.currentThread().getName();

	private final File file;

	public PageProducer(File file) {
		this.file = file;
	}

	@Override
	public void subscribe(FlowableEmitter<Page> emitter) throws Throwable {
		try {
			String filePath = file.getAbsolutePath();
			Log.info("Starting reading: " + filePath, NAME);
			List<String> words = readWords(filePath);
			List<Page> pages = splitInPages(emitter, words);
			Log.info("Reading completed, added " + pages.size() + " pages: " + filePath, NAME);
			emitter.onComplete();
		} catch (PdfParserException e) {
			throw new IllegalStateException(e);
		}
	}

	private List<String> readWords(final String filePath) throws PdfParserException {
		String parsedText = extractText(filePath);
		Matcher matcher = makeWordMatcher(parsedText);
		return extractWords(matcher, parsedText);
	}
	
	private List<Page> splitInPages(FlowableEmitter<Page> emitter, List<String> words) {
		List<Page> pages = new LinkedList<>();
		for (int i = 0; i < words.size(); i += PAGE_SIZE) {
			Page page = new Page(words.subList(i, Math.min(i + PAGE_SIZE, words.size())));
			pages.add(page);
			emitter.onNext(page);
		}
		return pages;
	}

	private String extractText(final String filePath) throws PdfParserException {
		PdfParser parser = new PdfParser(filePath);
		return parser.parse();
	}

	private Matcher makeWordMatcher(final String parsedText) {
		Pattern p = Pattern.compile("[\\w']+");
		return p.matcher(parsedText);
	}

	private List<String> extractWords(final Matcher matcher, final String parsedText) {
		List<String> words = new LinkedList<>();
		while (matcher.find()) {
			String word = parsedText.substring(matcher.start(), matcher.end());
			words.add(word.toLowerCase());
		}
		return words;
	}
}
