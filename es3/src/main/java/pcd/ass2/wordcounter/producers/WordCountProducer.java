package pcd.ass2.wordcounter.producers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.rxjava3.core.FlowableEmitter;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;
import pcd.ass2.logging.Log;
import pcd.ass2.wordcounter.structures.data.Page;

public class WordCountProducer implements FlowableOnSubscribe<Map<String, Integer>> {

	private static final String NAME = "PARSE_TEXT " + Thread.currentThread().getName();

	private final Page page;

	private final List<String> excludedWords;

	private final Map<String, Integer> countMap;

	public WordCountProducer(final Page page, final List<String> excludedWords) {
		this.page = page;
		this.excludedWords = excludedWords;
		this.countMap = new HashMap<>();
	}

	@Override
	public void subscribe(FlowableEmitter<Map<String, Integer>> emitter) throws Throwable {
		Map<String, Integer> wordCounts = countWordsInPage();
		emitter.onNext(wordCounts);
		emitter.onComplete();
	}

	private Map<String, Integer> countWordsInPage() {
		Log.info("Starting parsing page", NAME);
		for (String rawWord : page.getWords()) {
			String word = rawWord.toLowerCase();
			if (!word.isEmpty() && !excludedWords.contains(word)) {
				registerWordOccurence(word);
			}
		}
		Log.info("Finished parsing page", NAME);
		return countMap;
	}

	private void registerWordOccurence(String word) {
		Integer wordOccurencies = countMap.get(word);
		if (wordOccurencies != null) {
			countMap.put(word, wordOccurencies + 1);
		} else {
			countMap.put(word, 1);
		}
	}

}
