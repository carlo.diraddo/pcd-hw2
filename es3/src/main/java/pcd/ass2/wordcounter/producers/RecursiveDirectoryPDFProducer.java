package pcd.ass2.wordcounter.producers;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import io.reactivex.rxjava3.core.FlowableEmitter;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;

public class RecursiveDirectoryPDFProducer implements FlowableOnSubscribe<File> {

	private final Queue<File> directoriesToProcess;

	public RecursiveDirectoryPDFProducer(final File baseDirectory) {
		this.directoriesToProcess = new LinkedList<>();
		directoriesToProcess.add(baseDirectory);
	}

	@Override
	public void subscribe(FlowableEmitter<File> emitter) throws Throwable {
		while (!directoriesToProcess.isEmpty()) {
			processDirectory(emitter);
		}
		emitter.onComplete();
	}

	private void processDirectory(FlowableEmitter<File> emitter) {
		File directory = directoriesToProcess.poll();
		List<File> filesToProcess = Arrays.asList(directory.listFiles());
		for (File file : filesToProcess) {
			processFile(emitter, file);
		}
	}

	private void processFile(FlowableEmitter<File> emitter, File file) {
		if (file.isDirectory()) {
			directoriesToProcess.add(file);
		} else {
			publishIfReadablePdf(emitter, file);
		}
	}

	private void publishIfReadablePdf(FlowableEmitter<File> emitter, File file) {
		if (isReadablePdfFile(file)) {
			emitter.onNext(file);
		}
	}

	private boolean isReadablePdfFile(final File file) {
		return file.isFile() && file.canRead() && file.getName().endsWith(".pdf");
	}

}
