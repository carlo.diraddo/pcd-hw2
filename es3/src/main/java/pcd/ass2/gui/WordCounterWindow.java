package pcd.ass2.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.PublishSubject;
import pcd.ass2.logging.Log;
import pcd.ass2.wordcounter.service.WordCounterService;

public class WordCounterWindow {

	private PublishSubject<WordCounterWindowEvent> eventStream;

	private static final Integer WINDOW_WIDTH = 2400;
	private static final Integer WINDOW_HEIGHT = 600;

	private final JFrame frame;
	private final JPanel panel;
	private final JScrollPane scrollPane;

	private final JFileChooser filesDirectory;
	private final JTextField outputWordsNumber;
	private final JFileChooser exclusionFile;

	private WordCounterService wordCounter;

	public final JButton startButton;
	private final JButton stopButton;

	private final JTextField wordsCounted;
	private final JTextArea outputArea;

	public WordCounterWindow() {
		this.frame = new JFrame("PCD Assignment 1");
		setUpFrame();
		this.panel = new JPanel();
		setUpPanel();
		this.scrollPane = new JScrollPane(panel);
		setUpScrollPane();
		this.filesDirectory = addChooserWithLabel("PDF files directory: ");
		setUpFilesDirectory();
		this.exclusionFile = addChooserWithLabel("Optional path to the words exclusion file");
		this.outputWordsNumber = addFieldWithLabel("Number of the most frequent words in output: ");

		this.startButton = addButton("Start");
		this.stopButton = addButton("Stop");

		this.wordsCounted = addFieldWithLabel("Words Counted");
		this.wordsCounted.setEditable(false);
		this.outputArea = addTextAreaWithLabel("Words Occurencies");
		this.outputArea.setEditable(false);

		this.frame.pack();
		this.frame.setVisible(true);

		this.eventStream = initializeEventStream();
		bindButtonEvents();
		
		stopButton.setEnabled(false);
	}

	private PublishSubject<WordCounterWindowEvent> initializeEventStream() {
		PublishSubject<WordCounterWindowEvent> stream = PublishSubject.create();
		stream.observeOn(Schedulers.computation()).subscribe(this::processWindowEvent);
		return stream;
	}

	private void processWindowEvent(WordCounterWindowEvent event) {
		if (event.equals(WordCounterWindowEvent.START)) {
			fireStartEvent();
		} else {
			fireStopEvent();
		}
	}

	private void fireStartEvent() {
		try {
			resetTextFiledsBorderColors();
			verifyFieldsAreValid();
			clearLogAndOutput();
			startButton.setEnabled(false);
			compute();
		} catch (Exception e) {
			handleCountErrorException(e);
		}
	}

	private void fireStopEvent() {
		wordCounter.stopCount();
	}

	private void bindButtonEvents() {
		this.startButton.addActionListener((ActionEvent ev) -> eventStream.onNext(WordCounterWindowEvent.START));
		this.stopButton.addActionListener((ActionEvent ev) -> eventStream.onNext(WordCounterWindowEvent.STOP));
	}

	private void setUpFrame() {
		frame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

	private void setUpPanel() {
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
	}

	private void setUpScrollPane() {
		scrollPane.setPreferredSize(new Dimension(500, 500));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
	}

	private void setUpFilesDirectory() {
		filesDirectory.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		filesDirectory.setAcceptAllFileFilterUsed(false);
	}

	public void compute() {
		Integer mostFrequentWordsNumber = Integer.parseInt(outputWordsNumber.getText().trim());
		WordCounterParameters parameters = new WordCounterParameters();
		parameters.setDirectory(filesDirectory.getSelectedFile());
		parameters.setExclusionFile(exclusionFile.getSelectedFile());
		parameters.setMostFrequentWordsNumber(mostFrequentWordsNumber);
		parameters.setVirtualCoresNumber(Runtime.getRuntime().availableProcessors() - Thread.activeCount());

		startButton.setEnabled(false);
		stopButton.setEnabled(true);
		this.wordCounter = new WordCounterService(parameters, outputArea, wordsCounted, startButton);
		wordCounter.compute();
	}

	public void stop() {
		wordCounter.stopCount();
		stopButton.setEnabled(false);
	}

	private JTextArea addTextAreaWithLabel(String labelText) {
		JTextArea textArea = new JTextArea(5, 20);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setBorder(new LineBorder(Color.GRAY, 1));
		textArea.setBackground(Color.BLACK);
		textArea.setForeground(Color.WHITE);
		addComponentWithLabel(textArea, labelText);
		return textArea;
	}

	private JTextField addFieldWithLabel(String labelText) {
		JTextField field = new JTextField();
		addComponentWithLabel(field, labelText);
		return field;
	}

	private JFileChooser addChooserWithLabel(String labelText) {
		JFileChooser chooser = new JFileChooser();
		addComponentWithLabel(chooser, labelText);
		return chooser;
	}

	private void addComponentWithLabel(JComponent component, String labelText) {
		JLabel label = new JLabel(labelText);
		label.setLabelFor(component);

		panel.add(label);
		panel.add(component);
	}

	private JButton addButton(String buttonText) {
		JButton button = new JButton(buttonText);
		panel.add(button);
		return button;
	}

	public void handleCountErrorException(Exception e) {
		String message = String.format("An exception has occured when counting most frequent words: %s", e);
		Log.error(message, "MAIN");
		outputErrorInTable(e, message);
	}

	public void verifyFieldsAreValid() {
		if (filesDirectory.getSelectedFile() == null) {
			handleInvalidInput(filesDirectory, "Files directory must be specified!");
		}
		if (!filesDirectory.getSelectedFile().isDirectory()) {
			handleInvalidInput(filesDirectory, "Files directory must be a directory!");
		}
		if (outputWordsNumber.getText().trim().isEmpty()) {
			handleInvalidInput(outputWordsNumber, "Output number of words must be specified!");
		}
		if (!isInteger(outputWordsNumber.getText().trim())) {
			handleInvalidInput(outputWordsNumber, "Output number of words must be a number!");
		}
		if (exclusionFile.getSelectedFile() != null && !exclusionFile.getSelectedFile().isFile()) {
			handleInvalidInput(exclusionFile, "Provided exclusion file must be a file!");
		}
	}

	private void handleInvalidInput(JComponent component, String message) {
		component.setBorder(new LineBorder(Color.RED, 2));
		throw new IllegalArgumentException(message);
	}

	public void clearLogAndOutput() {
		outputArea.setText("");
	}

	private void outputErrorInTable(Exception e, String message) {
		outputArea.setText(String.format("Exception: %s, Message: %s", e, message));
	}

	private boolean isInteger(String text) {
		try {
			Integer.parseInt(text);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public void resetTextFiledsBorderColors() {
		filesDirectory.setBorder(new LineBorder(Color.GRAY, 1));
		outputWordsNumber.setBorder(new LineBorder(Color.GRAY, 1));
	}

}
