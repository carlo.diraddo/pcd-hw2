var express = require('express');
var router = express.Router();

var trainsLib = require('../lib/trains-lib');


router.get('/solutions', async function (req, res, next) {
    const startStation = req.body.departure;
    const endStation = req.body.destination;
    const date = req.body.date;
    const time = req.body.time;

    if (!startStation || !endStation || !date || !time || !isDateFormatValid(date) || !isTimeFormatValid(time)) {
        res.status(400).json({ status: 400, message: "Bad arguments" });
    }

    trainsLib.getTrainSolutions(startStation, endStation, date, time).then(async (solutions) => {
        res.status(200).json(solutions);
    }, (error) => {
        res.status(500).json({message: error});
    });

});

function isDateFormatValid(date) {
    const re = /[0-9]{2}\/[0-9]{2}\/[0-9]{2}/;
    return re.test(date);
}

function isTimeFormatValid(time) {
    const re = /[0-9]{2}/;
    return re.test(time);
}

router.get('/train/:idTrain', async function (req, res, next) {
    const idTrain = req.params.idTrain;
    trainsLib.getRealTimeTrainInfo(idTrain).then(async (info) => {
        res.status(200).json(info);
    }, (error) => {
        res.status(500).json({message: error});
    });;
});

router.get('/station/:idStation', async function (req, res, next) {
    const idStation = req.params.idStation;
    trainsLib.getRealTimeStationInfo(idStation).then(async (info) => {
        res.status(200).json(info);
    }, (error) => {
        res.status(500).json({message: error});
    });;
});

module.exports = router;