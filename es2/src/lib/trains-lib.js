const axios = require('axios');
const cheerio = require('cheerio');
const { response } = require('express');

const solutionsBase = "https://www.lefrecce.it/msite/api/solutions";
const infoBase = "http://www.viaggiatreno.it/vt_pax_internet/mobile";

/*
recupera l'elenco delle possibili soluzioni di viaggio fra una stazione di partenza e verso una stazione destinazione, in una specifica data e a partire da una certa ora 
startStation - string
endStation - string
date - string "dd/MM/yyyy"
time - string "HH"

https://www.lefrecce.it/msite/api/solutions?origin=CESENA&destination=BOLOGNA%20CENTRALE&arflag=A&adate=18/04/2021&atime=11&adultno=1&childno=0&direction=A&frecce=false&onlyRegional=false
https://www.lefrecce.it/msite/api/solutions/f1bb1f415507b6e7fcdab2aa835d370ci0/details
*/
function getTrainSolutions(startStation, endStation, date, time) {
    return new Promise(async (resolve, reject) => {
        const origin = startStation;
        const destionation = endStation;
        const arFlag = "A";
        const aDate = date;
        const aTime = time;
        const adultsNum = 1;
        const childNum = 0;
        const direction = "A";
        const frecce = false;
        const regional = false;

        var url = solutionsBase;
        url += "?origin=" + origin;
        url += "&destination=" + destionation;
        url += "&arflag=" + arFlag;
        url += "&adate=" + aDate;
        url += "&atime=" + aTime;
        url += "&adultno=" + adultsNum;
        url += "&childno=" + childNum;
        url += "&direction=" + direction;
        url += "&frecce=" + frecce;
        url += "&onlyRegional=" + regional;

        encodedUrl = encodeURI(url);

        console.log(`Calling: ${encodedUrl}`);

        try {
            const response = await axios.get(encodedUrl);
            solutions = response.data;
            resolve(solutions.map(solution => formSolutionDto(solution)));
        } catch (err) {
            reject(err)
        }
    })
}

function formSolutionDto(solution) {
    return {
        "idSolution": solution.idsolution,
        "origin": solution.origin,
        "destination": solution.destination,
        "departuretime": new Date(solution.departuretime),
        "arrivalTime": new Date(solution.arrivaltime),
        "price": solution.minprice,
        "duration": solution.duration,
        "numberOfChanges": solution.changesno,
        "trains": solution.trainlist.map(train => formTrainDto(train)),
        "trainIds": solution.trainlist.map(train =>formNumberFromTrain(train))
    };
}

function formTrainDto(train) {
    const re = /[0-9]+$/

    return {
        "name": train.trainidentifier,
        "number": train.trainidentifier.match(re)[0]
    }
}

function formNumberFromTrain(train) {
    const re = /[0-9]+$/

    return train.trainidentifier.match(re)[0]
}

/*
recupera le informazioni circa lo stato corrente di uno specifico treno

html page to scrape
GET http://www.viaggiatreno.it/vt_pax_internet/mobile/scheda?numeroTreno=[NUMEROTRENO]
*/
function getRealTimeTrainInfo(trainId) {
    return new Promise(async (resolve, reject) => {
        var url = infoBase
        url += "/scheda?numeroTreno=" + trainId;

        console.log(`Calling: ${url}`);

        const response = await axios.get(url);
        if (response.status === 200) {
            const html = response.data;
            const $ = cheerio.load(html);
            resolve(parseTrainInfo($, trainId))
        } else {
            reject(response.status);
        }
    });
}

function parseTrainInfo($, trainId) {

    dto = {}
    dto.trainNumber = trainId;
    dto.situation = parseTrainSituation($);

    if (dto.situation && dto.situation != "Il treno non e' ancora partito") {
        dto.departure = parseTrainDeparture($);
        dto.lastStop = parseTrainLastStop($);
        dto.arrival = parseTrainArrival($);
    }

    return dto;
}

function parseTrainSituation($) {
    const text = getContainerByCommentAsText($, ' SITUAZIONE ')
    if(text == "Il treno non e' ancora partito") {
        return text;
    }
    return {
        "minutesLate": text.slice(0, text.indexOf('Ultimo')).match(/[0-9]+/)[0].trim(),
        "lastPing": text.slice(text.indexOf('Ultimo rilevamento a') + 'Ultimo rilevamento a'.length).trim(),
    };
}

function parseTrainDeparture($) {
    const text = getContainerByCommentAsText($, ' ORIGINE ');
    return {
        "station": text.slice(0, text.indexOf('Partenza')).trim(),
        "scheduledTime": text.slice(text.indexOf('programmata :') + 'programmata :'.length, text.indexOf('Partenza effettiva')).trim(),
        "actualTime": text.slice(text.indexOf('Partenza effettiva:') + 'Partenza effettiva :'.length, text.indexOf('Binario Previsto')).trim(),
        "scheduledPlatform": text.slice(text.indexOf('Binario Previsto:') + 'Binario Previsto:'.length, text.indexOf('Binario Reale')).trim(),
        "actualPlatform": text.slice(text.indexOf('Binario Reale:') + 'Binario Reale:'.length).trim(),
    };
}

function parseTrainLastStop($) {
    // 'ROMA TERMINI Arrivo Programmato: 15:50 Arrivo effettivo: 15:52 Binario Previsto: 1 EST Binario Reale: --'
    const text = getContainerByComment($, ' ULTIMA FERMATA ').next().contents().text().replace(/\s+/g, ' ').trim();
    return {
        "station": text.slice(0, text.indexOf('Arrivo')).trim(),
        "scheduledTime": text.slice(text.indexOf('Arrivo Programmato:') + 'Arrivo Programmato:'.length, text.indexOf('Arrivo effettivo')).trim(),
        "actualTime": text.slice(text.indexOf('Arrivo effettivo:') + 'Arrivo effettivo:'.length, text.indexOf('Binario Previsto')).trim()
    };
}

function parseTrainArrival($) {
    // 'ROMA TERMINI Arrivo Programmato: 15:50 Arrivo effettivo: 15:52 Binario Previsto: 1 EST Binario Reale: --'
    const text = getContainerByCommentAsText($, ' DESTINAZIONE ');
    return {
        "station": text.slice(0, text.indexOf('Arrivo')).trim(),
        "scheduledTime": text.slice(text.indexOf('Arrivo Programmato:') + 'Arrivo Programmato:'.length, text.indexOf('Arrivo effettivo')).trim(),
        "actualTime": text.slice(text.indexOf('Arrivo effettivo:') + 'Arrivo effettivo:'.length, text.indexOf('Binario Previsto')).trim(),
        "scheduledPlatform": text.slice(text.indexOf('Binario Previsto:') + 'Binario Previsto:'.length, text.indexOf('Binario Reale')).trim(),
        "actualPlatform": text.slice(text.indexOf('Binario Reale:') + 'Binario Reale:'.length).trim(),
    };
}

function getContainerByCommentAsText($, comment) {
    return getContainerByComment($, comment).contents().text().replace(/\s+/g, ' ').trim();
}

function getContainerByComment($, comment) {
    const tag = getComments($).filter(function () {
        return this.data == comment;
    });
    return tag.next();
}

function getComments($) {
    return $("body").contents().filter(function () {
        return this.nodeType === 8;
    });
}

/*
recupera le informazioni circa lo stato corrente di una specifica stazione

html page to scrape
POST
http://www.viaggiatreno.it/vt_pax_internet/mobile/stazione?codiceStazione=[CODICE STAZIONE]
*/
function getRealTimeStationInfo(stationId) {
    return new Promise(async (resolve, reject) => {
        var url = infoBase
        url += "/stazione?codiceStazione=" + stationId;

        console.log(`Calling: ${url}`);

        const response = await axios.post(url);
        if (response.status === 200) {
            const html = response.data;
            const $ = cheerio.load(html);
            resolve(parseStationInfo($))
        } else {
            reject(response.status);
        }
    });
}

function parseStationInfo($) {
    const departures = getSeriesOfContainersByComment($, ' RISULTATI RICERCA - PARTENZE ')
    const arrivals = getSeriesOfContainersByComment($, ' RISULTATI RICERCA - ARRIVI ')

    departuresParsed = [];
    departures.forEach((item) => {
        departuresParsed.push(parseBlock(item.text().replace(/\s+/g, ' ').trim()))
    });

    arrivalsParsed = [];
    arrivals.forEach((item) => {
        arrivalsParsed.push(parseBlock(item.text().replace(/\s+/g, ' ').trim()))
    });
    
    return {
        "departures": departuresParsed,
        "arrivals": arrivalsParsed
    };
}

function getSeriesOfContainersByComment($, comment) {
    containers = [];
    var container = getContainerByComment($, comment);
    while(container[0] && container[0].name == "div" && container[0].attribs.class == "bloccorisultato") {
        containers.push(container);
        container = container.next();
    }
    return containers;
}

function parseBlock(text) {
    if(text.includes("ritardo")) {
        actualPlatform = text.slice(text.indexOf('Binario Reale:') + 'Binario Reale:'.length, text.indexOf('ritardo')).trim();
        minutesLate = text.slice(text.indexOf('ritardo') + 'ritardo'.length, text.indexOf("»")).trim();
    } else {
        actualPlatform = text.slice(text.indexOf('Binario Reale:') + 'Binario Reale:'.length, text.indexOf('in orario')).trim();
        minutesLate = "0"
    }

    if(text.includes(" Per ")) {
        trainName = text.slice(0, text.indexOf('Per')).trim();
        destination = text.slice(text.indexOf('Per') + 'Per'.length, text.indexOf('Delle ore')).trim();
    } else {
        trainName = text.slice(0, text.indexOf('Da')).trim();
        destination = text.slice(text.indexOf('Da') + 'Da'.length, text.indexOf('Delle ore')).trim();
    }

    return {
        "name": trainName,
        "destination": destination,
        "scheduledTime": text.slice(text.indexOf('Delle ore') + 'Delle ore'.length, text.indexOf('Binario Previsto')).trim(),
        "scheduledPlatform": text.slice(text.indexOf('Previsto:') + 'Previsto:'.length, text.indexOf('Binario Reale')).trim(),

        "actualPlatform": actualPlatform,
        "minutesLate": minutesLate,
    }
}

exports.getTrainSolutions = getTrainSolutions;
exports.getRealTimeTrainInfo = getRealTimeTrainInfo;
exports.getRealTimeStationInfo = getRealTimeStationInfo;