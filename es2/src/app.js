const express = require('express');
const path = require('path');
const trainsRouter = require('./routes/trains');
const trainsLib = require('./lib/trains-lib');
const {
    timeStamp
} = require('console');

const PORT = 3030;

// Train Monitor (Singleton)

class TrainMonitor {
    constructor() {
        this.trains = [];
        this.io = null;
        this.intervalId = null;
        this.isPaused = false;
        this.status = {};
    }

    requestMonitoring(trains, io) {
        if (this.intervalId) {
            return;
        }

        this.isPaused = false;
        this.trains = trains;
        this.io = io;

        console.log("Monitoring Trains: " + trains)
        this.intervalId = setInterval(async function () {
            this.pushMonitor();
        }.bind(this), 4000);
    }

    async pushMonitor() {
        let status = this.status;
        status.trains = [];
        if (!this.isPaused) {
            this.status.status = "Running";
            for (let i = 0; i < trains.length; i++) {
                let train = trains[i];
                const trainInfo = await trainsLib.getRealTimeTrainInfo(train);
                let isDeparted = (trainInfo.situation != "Il treno non e' ancora partito");

                if (!status.trains[i]) {
                    // initialize new train
                    status.trains[i] = {
                        id: trainInfo.trainNumber,
                        isDeparted: isDeparted,
                        minutesLate: trainInfo.situation.minutesLate,
                        departure: trainInfo.departure,
                        lastStop: [trainInfo.lastStop],
                        arrival: trainInfo.arrival
                    }
                } else {
                    // train exists, update info
                    status.trains[i].isDeparted = isDeparted;
                    if(isDeparted) {
                        status.trains[i].departure = trainInfo.departure;
                        status.trains[i].arrival = trainInfo.arrival;

                        if(trainInfo.lastStop.station != status.trains[i].lastStop[status.trains[i].lastStop.length - 1].station) {
                            status.trains[i].lastStop.push(trainInfo.lastStop);
                        }
                    }
                }
            }

            this.status = status;
            io.emit('trains-status', JSON.stringify(this.status));
        } else {
            status.status = "Paused";
        }
    }

    pauseMonitoring() {
        console.log("Monitoring paused");
        this.isPaused = true;
    }

    resumeMonitoring() {
        console.log("Monitoring resumed");
        this.isPaused = false;
    }

    resetMonitoring() {
        console.log("Monitoring reset");
        clearInterval(this.intervalId);
        this.intervalId = null;

        this.status = {};
    }
}

const trainMonitor = new TrainMonitor();

// Express

const app = express();

app.set('views', path.join(__dirname, '../', 'views'));
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/trains/', trainsRouter);

app.get('/', function (req, res, next) {
    res.render('search', {
        title: 'PCD Assignment 2'
    });
});

app.post('/solutions', async function (req, res, next) {
    const startStation = req.body.startStation;
    const endStation = req.body.endStation;
    const date = req.body.date;
    const time = req.body.time;

    if (!startStation || !endStation || !date || !time || !isDateFormatValid(date) || !isTimeFormatValid(time)) {
        res.status(400).json({
            status: 400,
            message: "Bad arguments"
        });
    }

    trainsLib.getTrainSolutions(startStation, endStation, date, time).then(async (solutions) => {
        res.render('solution-list', {
            title: 'PCD Assignment 2',
            solutions: solutions
        });
    }, (error) => {
        console.error(error)
        res.render('solution-list', {
            title: 'PCD Assignment 2',
            solutions: []
        });
    });

});

app.get('/solution-detail', function (req, res, next) {
    trains = req.query.trains.split(",");
    res.render('solution-detail', {
        title: 'PCD Assignment 2',
        trains: trains
    });
});

app.listen(PORT, () => {
    console.log(`App listening at http://localhost:${port}`);
})

// WEB SOCKET

const httpServer = require("http").createServer(app);
const options = {
    cors: {
        origin: '*',
    }
};
const io = require("socket.io")(httpServer, options);

io.on("connection", socket => {
    console.log("Client Connected!");
    socket.on('monitoring', (trains) => {
        trainMonitor.requestMonitoring(trains.split(","), io);
    });

    socket.on('pause', () => {
        console.log("Pause Request Received");
        trainMonitor.pauseMonitoring();
    });

    socket.on('resume', () => {
        console.log("Resume Request Received");
        trainMonitor.resumeMonitoring();
    });

    socket.on('reset', () => {
        console.log("Reset Request Received");
        trainMonitor.resetMonitoring();
    });
});

httpServer.listen(3000, () => {
    console.log('WebSocket listening on 3000');
});

function isDateFormatValid(date) {
    const re = /[0-9]{2}\/[0-9]{2}\/[0-9]{2}/;
    return re.test(date);
}

function isTimeFormatValid(time) {
    const re = /[0-9]{2}/;
    return re.test(time);
}

module.exports = app;