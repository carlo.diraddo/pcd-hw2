package pcd.ass2.wordcounter.agent.task;

import java.io.File;

import pcd.ass2.wordcounter.service.WordCounterService;
import pcd.ass2.wordcounter.structures.data.Page;

/**
 * Task factory that abstarcts the dependencies injection;
 * @author Cyrill
 *
 */
public class ServiceTaskFactory {
	
	private final WordCounterService service;

	public ServiceTaskFactory(final WordCounterService service) {
		this.service = service;
	}
	
	public RecursiveFolderPDFSearchTask recursiveFolderPDFSearchTask(final File baseDirectory) {
		RecursiveFolderPDFSearchTask task = new RecursiveFolderPDFSearchTask(this, baseDirectory);
		service.getRunningTasks().add(task);
		return task;
	}
	
	public ReadPDFTask readPDFTask(final File pdfFile) {
		ReadPDFTask task = new ReadPDFTask(this, pdfFile);
		service.getRunningTasks().add(task);
		return task;
	}
	
	public CountWordsTask countWordsTask(final Page page) {
		CountWordsTask task = new CountWordsTask(service, page);
		service.getRunningTasks().add(task);
		return task;
	}
	
}
